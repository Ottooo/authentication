package com.example.autentication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_signin.*
import kotlinx.android.synthetic.main.activity_signup.*

class SignInActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signin)
        init()
    }

    private fun init() {
        auth = Firebase.auth
        SignInButton.setOnClickListener {
            signIn()
        }

        }
    public fun signIn() {
        val signInEmail = SignInEmailEditText.text.toString()
        val signInPassword = SignInPasswordEditText.text.toString()
        if (signInPassword.isNotEmpty() && signInEmail.isNotEmpty()) {
            signInProgressBar.visibility = View.VISIBLE
            auth.signInWithEmailAndPassword(signInEmail, signInPassword)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {

                        Toast.makeText(baseContext, "Authentication is Success!", Toast.LENGTH_LONG)
                            .show()
                        val user = auth.currentUser
                        signInProgressBar.visibility = View.GONE
                    } else {

                        Toast.makeText(baseContext, "Authentication failed.", Toast.LENGTH_SHORT)
                            .show()
                    }

                }


        }
    }
}