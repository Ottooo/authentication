package com.example.autentication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.autentication_activity.*

class AuthenticationActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.autentication_activity)
        init()
    }
    private fun init (){
        SignInButton.setOnClickListener {
             val intent = Intent( this , SignInActivity::class.java)
            startActivity(intent)
        }
        SignUpButton.setOnClickListener {
            val intent = Intent(this,SignupActivity::class.java)
            startActivity(intent)

        }


        }

    }
